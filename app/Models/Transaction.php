<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;
    protected $table = 'transactions';
    protected $fillable = [
        'name',
        'country',
        'city',
        'street',
        'province',
        'postal_code',
        'phone',
        'email',
        'note',
        'total_price'
    ];
}
