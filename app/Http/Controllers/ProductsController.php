<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use App\Models\ProductImage;
use App\Models\Products;
use App\Models\Category;
use App\Models\Subcategory;
use App\Models\ShoppingCart;
use Illuminate\Http\Request;
use File; 
// use Illuminate\Support\Facades\DB;

class ProductsController extends Controller
{
    public function index(){
        $products = Products::select('id', 'name', 'description', 'price', 'thumbnail')->get();

        return response()->json($products, 201);
    }

    public function add_product(Request $request){
        $price = (int)$request->price;
        $category_id = (int)$request->category_id;
        $subcategory_id = (int)$request->subcategory_id;

        // Thumbnail
        $explode_thumbnail = explode(',', $request->thumbnail);
        $decode_thumbnail = base64_decode($explode_thumbnail[1]);
        
        if(str_contains($explode_thumbnail[0], 'jpeg')){
            $extentsion = 'jpg';
        }else{
            $extentsion = 'png';
        }

        $str_random = time();
        $fileName = $str_random.'.'.$extentsion;

        $path = public_path().'/image/products/'.$fileName;

        $put_image = file_put_contents($path, $decode_thumbnail);
        if($put_image){
            $product = Products::create([
                'name' => $request->product_name,
                'thumbnail' => '/image/products/'.$fileName,
                'price' => $price,
                'description' => $request->description,
                'category_id' => $category_id,
                'subcategory_id' => $subcategory_id
            ]);
            $get_product = Products::where('name', $request->product_name)->first();
            $product_id = $get_product->id;
        }

        // Images
        foreach($request->images as $image){
            $explode = explode(',', $image);
            $decode = base64_decode($explode[1]);

            if(str_contains($explode[0], 'jpeg')){
                $extentsion = 'jpg';
            }else{
                $extentsion = 'png';
            }

            $str_random = time();
            $fileName = $str_random.'.'.$extentsion;

            $path = public_path().'/image/products/'.$fileName;

            $put_image = file_put_contents($path, $decode);
            if($put_image){
                $product_image = new ProductImage;
                $product_image->product_id = $product_id;
                $product_image->image = '/image/products/'.$fileName;
                $product_image->save();
            }
        }

        return response()->json("success", 201);
    }

    public function update_product(Request $request){
        $price = (int)$request->price;
        $category_id = (int)$request->category_id;
        $subcategory_id = (int)$request->subcategory_id;

        $product = Products::where('name', $request->old_product_name)->update([
            'name' => $request->product_name,
            'price' => $price,
            'description' => $request->description,
            'category_id' => $category_id,
            'subcategory_id' => $subcategory_id
        ]);

        $get_product = Products::where('name', $request->product_name)->first();
        $product_id = $get_product->id;
        
        // Delete Old Thumbnail
        if(isset($request->old_thumbnail)){
            $file = substr($request->old_thumbnail, 1);
                $file_path = $file;
                if(File::exists($file_path)){
                    File::delete($file_path);
                    // Thumbnail
                    $explode_thumbnail = explode(',', $request->thumbnail);
                    $decode_thumbnail = base64_decode($explode_thumbnail[1]);
                    
                    if(str_contains($explode_thumbnail[0], 'jpeg')){
                        $extentsion = 'jpg';
                    }else{
                        $extentsion = 'png';
                    }

                    $str_random = time();
                    $fileName = $str_random.'.'.$extentsion;

                    $path = public_path().'/image/products/'.$fileName;

                    $put_image = file_put_contents($path, $decode_thumbnail);
                    if($put_image){
                        $product = Products::where('name', $request->old_product_name)->update([
                            'thumbnail' => '/image/products/'.$fileName
                        ]);
                    }
                }
        }
        // Images
        if(isset($request->images)){
            foreach($request->images as $image){
                $explode = explode(',', $image);
                $decode = base64_decode($explode[1]);
    
                if(str_contains($explode[0], 'jpeg')){
                    $extentsion = 'jpg';
                }else{
                    $extentsion = 'png';
                }
    
                $str_random = time();
                $fileName = $str_random.'.'.$extentsion;
    
                $path = public_path().'/image/products/'.$fileName;
    
                $put_image = file_put_contents($path, $decode);
                if($put_image){
                    $product_image = new ProductImage;
                    $product_image->product_id = $product_id;
                    $product_image->image = '/image/products/'.$fileName;
                    $product_image->save();
                }
            }
        }
        return response()->json("Successfully Updated!", 201);
    }

    public function show($category){
        $category_result = Category::where('category', $category)->first();
        $products = Products::where('category_id', $category_result->id)->paginate(10);
        foreach($products as $product){
            $images = ProductImage::select('image')->where('product_id', $product->id)->get();
            $subcategory = Subcategory::where('id', $product->subcategory_id)->first();
            $product->subcategory = $subcategory->subcategory;
            $i = 0;
            foreach($images as $img){
                $im[$i] = $img->image; 
                $i++;
            }
            $product->image = $im;
        }
        return response()->json($products, 201);
    }

    public function get_products_by_subcategory($subcategory_name){
        $get_subcategory = Subcategory::where('subcategory', $subcategory_name)->first();
        $products = Products::where('subcategory_id', $get_subcategory->id)->get();
        foreach($products as $product){
            $product->subcategory = $subcategory_name;
            $images = ProductImage::select('image')->where('product_id', $product->id)->get();
            $i = 0;
            foreach($images as $img){
                $im[$i] = $img->image; 
                $i++;
            }
            $product->image = $im;
        }
        return response()->json($products, 201);
    }

    public function product($product_name){
        $product = Products::where('name', $product_name)->first();
        $images = ProductImage::where('product_id', $product->id)->get();
        $i = 0;
        foreach($images as $image){
            $im[$i] = $image->image;
            $i++;
        }
        $product->image = $im;
        return response()->json($product, 201);

    }

    public function delete_product($selected){
        $selected_array = explode(',', $selected);
        foreach($selected_array as $name){
            $get_product = Products::where('name', $name)->first();

            $product_images = ProductImage::where('product_id', $get_product->id)->get();

            $file = substr($get_product->thumbnail, 1);
                $file_path = $file;
                if(File::exists($file_path)){
                    File::delete($file_path);
            }

            foreach($product_images as $product_image){
                $file = substr($product_image->image, 1);
                $file_path = $file;
                if(File::exists($file_path)){
                    File::delete($file_path);
                }
            }

            ProductImage::where('product_id', $get_product->id)->delete();
            ShoppingCart::where('product_id', $get_product->id)->delete();
            Products::where('name', $name)->delete();
        }

        return response()->json($selected_array, 201);
    }

    public function search_product($category, $product_name){
        $get_category = Category::where('category', $category)->first();
        $products = Products::select('*')->where('name', 'like', '%' . $product_name . '%')->where('category_id', $get_category->id)->get();
        foreach($products as $product){
            $images = ProductImage::select('image')->where('product_id', $product->id)->get();
            $i = 0;
            foreach($images as $img){
                $im[$i] = $img->image; 
                $i++;
            }
            $product->image = $im;
        }
        return response()->json($products, 201);
    }
}
