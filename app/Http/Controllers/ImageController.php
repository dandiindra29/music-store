<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProductImage;
use File; 

class ImageController extends Controller
{
    public function delete_product_image($image){
        $act = ProductImage::where('image', '/image/products/' . $image)->delete();
        if($act){
            $file = 'image/products/' . $image;
                $file_path = $file;
                if(File::exists($file_path)){
                    File::delete($file_path);
                    return response()->json("Successfully Deleted!", 201);
                }
            
        }
    }
}
