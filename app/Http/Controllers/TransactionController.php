<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use Illuminate\Support\Facades\Auth;

class TransactionController extends Controller
{
    public function index(){
        $transaction = Transaction::all();

        return response()->json($transaction, 201);
    }

    public function get_user_transaction($user_id){
        $transactions = Transaction::where('user_id', $user_id)->get();

        foreach($transactions as $transaction){
            $transaction_detail = TransactionDetail::where('transaction_id', $transaction->id)->get();
            $transaction->detail = $transaction_detail;
        }

        return response()->json($transactions, 201);
    }   

    public function get_transaction_detail($id){
        $transaction = Transaction::where('id', $id)->first();

        $transaction_detail = TransactionDetail::where('transaction_id', $transaction->id)->get();
        $transaction->detail = $transaction_detail;

        return response()->json($transaction, 201);
    }   

    public function create(Request $request){
        $transaction = Transaction::create([
            'name' => $request->name,
            'country' => $request->country,
            'city' => $request->city,
            'street' => $request->street,
            'province' => $request->province,
            'postal_code' => $request->postal_code,
            'phone' => $request->phone,
            'email' => $request->email,
            'note' => $request->note,
            'total_price' => $request->total_price
        ]);
        
        for( $i = 0 ; $i < count($request->products) ; $i++){
            $transaction_detail = TransactionDetail::create([
                'transaction_id' => $transaction->id,
                'product_id' => $request->products_id[$i],
                'product_name' => $request->products[$i],
                'quantity' => $request->quantities[$i],
                'price' => $request->prices[$i]
            ]);
        }

        return response()->json("Transaction Success!", 201);
    }
}
