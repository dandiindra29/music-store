<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Subcategory;
use App\Models\Category;

class SubcategoryController extends Controller
{
    public function index($category_name){
        $category = Category::where('category', $category_name)->first();
        $subcategory = Subcategory::where('category_id', $category->id)->get();

        return response()->json($subcategory, 201);
    }

    public function admin_get($category_id){
        $subcategory = Subcategory::where('category_id', $category_id)->get();

        return response()->json($subcategory, 201);
    }
}
