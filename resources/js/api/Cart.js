
export const addToCart = (userId, productId, quantity, userAccessToken) => {
    let formData = new FormData();
        formData.append('user_id', userId);
        formData.append('product_id', productId);
        formData.append('quantity', quantity);
    return axios.post('/api/shopping_cart/', formData, {
        headers: {
          Authorization: 'Bearer ' + userAccessToken
        }
    });
}

export const fetchCart = (userId, userAccessToken) => {
    return axios.get(`/api/shopping_cart/${userId}`, {
        headers: {
            Authorization: 'Bearer ' + userAccessToken
        }
    })
}