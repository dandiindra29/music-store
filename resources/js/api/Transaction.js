
export const makeTransaction = (carts, name, country, city, street, province, postalCode, phone, email, note, totalPrice) => {
    let fd = new FormData();

        for(let i=0 ; i < carts.length; i++){
            fd.append('products_id[]', carts[i].product_id)
            fd.append('products[]', carts[i].name);
            fd.append('quantities[]', carts[i].quantity);
            fd.append('prices[]', carts[i].price);
        }

        fd.append('name', name);
        fd.append('country', country);
        fd.append('city', city);
        fd.append('street', street);
        fd.append('province', province);
        fd.append('postal_code', postalCode);
        fd.append('phone', phone);
        fd.append('email', email);
        fd.append('note', note);
        fd.append('total_price', totalPrice);

    return axios.post('/api/transaction', fd)
}

export const transactionComplete = (userId, userAccessToken) => {
    return axios.get(`/api/transaction/${userId}`, {
        headers: { 'Authorization' : 'Bearer '+ userAccessToken}
    })
}

export const transactionCompleteDetail = (transactionId, userAccessToken) => {
    return axios.get(`/api/transaction/detail/${transactionId}`, {
        headers: { 'Authorization' : 'Bearer '+ userAccessToken}
    })
}