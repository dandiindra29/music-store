export const allProducts = (category) => {
    return axios.get(`/api/products/category/${category}`);
}

export const fetchProduct = (productName) => {
    return axios.get(`/api/product/${productName}`);
}

export const productsBySubcategory = (subcategory) => {
    return axios.get(`/api/products/subcategory/${subcategory}`);
}

export const fecthSubcategory = (category) => {
    return axios.get(`/api/subcategory/${category}`);
}

export const searchProduct = (category, search) => {
    return axios.get(`/api/product/search/${category}/${search}`);
}