import React, { useEffect, useState } from 'react';
import { withRouter, useParams } from 'react-router-dom';
import styled from 'styled-components';
import { TextField, FormLabel, Button, Grid, Card, MenuItem, CircularProgress } from '@material-ui/core';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import { makeStyles } from '@material-ui/core/styles';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Alert from '@material-ui/lab/Alert';

import AdminTemplate from '../../template/Admin';

const useStyles = makeStyles((theme) => ({
    root: {
        minWidth: 400
    },
    input: {
        marginTop: 20
    },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));


const StyledAddProduct = styled.div`
    background-color: white;
    padding: 20px;
    width: 100%;
`;

const StyledDelete = styled.div`
    color: red;
    &:hover{
        cursor: pointer;
    }
`;

const EditProduct = () => {
    
    const classes = useStyles();
    const [images, setImages] = useState([]);
    const [newImages, setNewImages] = useState([]);
    const [productName, setProductName] = useState('');
    const [oldProductName, setOldProductName] = useState('');
    const [price, setPrice] = useState(0);
    const [description, setDescription] = useState('');
    const [category, setCategory] = useState([]);
    const [subcategory, setSubcategory] = useState([]);
    const [categoryId, setCategoryId] = useState(0);
    const [subcategoryId, setSubcategoryId] = useState(0);
    const [saved, setSaved] = useState(false);
    const [fail, setFail] = useState(false);
    const [thumbnail, setThumbnail] = useState('');
    const [newThumbnail, setNewThumbnail] = useState('');
    const [progress, setProgress] = useState(false);
    const { name } = useParams();

    
    const getProduct = (name) => {
        axios.get(`/api/product/${name}`)
            .then(response => {
                const data = response.data;
                console.log(response.data)
                // setProductName(data.name);
                setOldProductName(data.name);
                setPrice(data.price);
                setDescription(data.description);
                setCategoryId(data.category_id);
                setSubcategoryId(data.subcategory_id);
                setThumbnail(data.thumbnail);
                setImages(data.image);
            });
    }

    useEffect(() => {
        setProductName(name);
        axios.get('/api/category')
        .then(response => {
            setCategory(response.data);
            getSubcategory(response.data[0].id);
            getProduct(name);
        });

    }, [])

    const getSubcategory = (category_id) => {
        axios.get(`/api/admin/subcategory/${category_id}`)
        .then(response => {
            setSubcategory(response.data)
        })
    }

    const handleOnChangeCategory = e => {
        setCategoryId(e.target.value);
        getSubcategory(e.target.value);
    }

    const handleOnChangeSubcategory = e => {
        setSubcategoryId(e.target.value);
    }

    const fileSelectedHandler = e => {
        let imagesArray = [];

        for( let i = 0 ; i < e.target.files.length ; i++){
            let fileReader = new FileReader();
            fileReader.readAsDataURL(e.target.files[i]);
            fileReader.onload = (e) => {
                imagesArray.push(e.target.result);
                setNewImages(imagesArray);
            }
        }
    }

    const thumbnailSelectedHandler = e => {
        let fileReader = new FileReader();
        fileReader.readAsDataURL(e.target.files[0]);
        fileReader.onload = e => {
            setNewThumbnail(e.target.result);
        }
    }

    const handleOnSubmit = () => {

        if( categoryId === 0 || productName === '' || price === 0 || description === '' || subcategoryId === 0 ){
            alert('Please fill Category, Subcategory, Price, Name, and Descritpion fields')
        }else if(thumbnail === '' && newThumbnail === ''){
            alert("Thumbnail Can't be Empty!")
        } else{
            setProgress(true);
            const fd = new FormData();

            if(newImages.length > 0){
                for(let i=0 ; i<newImages.length ; i++){
                    fd.append(`images[]`, newImages[i]);
                }
            }
            
            if(newThumbnail !== ''){
                fd.append('old_thumbnail', thumbnail);
                fd.append('thumbnail', newThumbnail);
            }

            fd.append('old_product_name', oldProductName);
            fd.append('category_id', categoryId);
            fd.append('subcategory_id', subcategoryId);
            fd.append('product_name', productName);
            fd.append('price', price);
            fd.append('description', description);
            const config = {
                headers: {
                    'content-type': 'multipart/form-data'
                }
            }

            axios.post('/api/update/product', fd, config)
            .then(response => {
                console.log(response.data)
                setProgress(false);
                setFail(false);
                setImages([]);
                setThumbnail('')
                getProduct(productName);
                setSaved(true);
            })
            .catch(response => {
                setProgress(false)
                setSaved(false);
                setFail(true);
            })
        }
        
    }

    return (
        <AdminTemplate>
                <StyledAddProduct>
                    <center>
                        <h3>Add a Product</h3>
                        <hr />
                        
                        <form >
                        <TextField type="text" placeholder="Product Name" value={productName} required onChange={(e) => {
                            setProductName(e.target.value);
                        }} />
                        <br />
                        <TextField className={classes.input} type="number" placeholder="Price" value={price} required onChange={e => {
                            setPrice(e.target.value);
                        }} />

                         <br />
                        <FormLabel className={classes.input}>
                            Thumbnail
                        </FormLabel>
                        <br />


                        <input type="file" onChange={thumbnailSelectedHandler} />

                        <center>
                            <Card style={{ marginTop: '20px', width: '50%' }}>
                                <img src={thumbnail} style={{ width: '100%' }} />
                            </Card>
                        </center>

                        <br />
                        <FormLabel className={classes.input}>
                            Images:
                        </FormLabel>
                        <br />


                        <input type="file" multiple onChange={fileSelectedHandler} />
                        <br />
                        <center>
                        {   
                            progress && <CircularProgress style={{ margin: '5px', color: 'black' }} />
                        }
                        </center>
                        <Grid container spacing={1} style={{ marginTop: '20px' }}>
                            {
                                images.map((image, key) => {
                                    return(
                                        <Grid key={key} item xs={4} sm={4} md={4} lg={4}>
                                            <center>
                                                <StyledDelete onClick={() => {
                                                    setProgress(true)
                                                    let res = image.split('/');
                                                    axios.delete(`/api/image/product/${res[3]}`)
                                                    .then(response => {
                                                        let imagesAfterDelete = images.filter(img => {
                                                            return img != image;
                                                        });
                                                        setProgress(false)
                                                        setImages(imagesAfterDelete);
                                                        console.log(response.data)
                                                    })
                                                    .catch(() => {
                                                        setProgress(false)
                                                    })
                                                }}>
                                                    <DeleteForeverIcon />
                                                </StyledDelete>
                                                
                                            </center>
                                            <Card>
                                                <img src={image} style={{ width: '100%' }} />
                                            </Card>
                                        </Grid>
                                    )
                                })
                            }
                        </Grid>

                        <br />
                        <FormControl className={classes.formControl}>
                            <InputLabel id="demo-simple-select-label">Category</InputLabel>
                            <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            onChange={handleOnChangeCategory}
                            value={categoryId}
                            >
                            {
                                category.map((c, key) => {

                                    return <MenuItem key={key} value={c.id}>{c.category}</MenuItem>
                                })
                            }
                            </Select>
                        </FormControl>
                        <br />
                        <FormControl className={classes.formControl}>
                            <InputLabel id="demo-simple-select-label">Subcategory</InputLabel>
                            <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            onChange={handleOnChangeSubcategory}
                            value={subcategoryId}
                            >
                            {
                                subcategory.map((c, key) => {

                                    return <MenuItem key={key} value={c.id}>{c.subcategory}</MenuItem>
                                })
                            }
                            </Select>
                        </FormControl>
                        <br />

                        <FormLabel className={classes.input}>
                            Description:
                        </FormLabel>
                        <br />
                        <br />
                        <CKEditor
                            className={classes.input}
                            editor={ ClassicEditor }
                            config={{
                                removePlugins: ['Image', 'ImageCaption', 'ImageStyle', 'ImageToolbar', 'ImageUpload', 'MediaEmbed']
                              }}
                            data={description}
                            onChange={ ( event, editor ) => {
                                const data = editor.getData();
                                setDescription(data);
                            } }
                        />
                        </form>
                    </center>
                    <br />
                    {
                        saved && <Alert severity="success"><strong>Saved!</strong></Alert>
                    }

                    {   
                        progress && <CircularProgress style={{ margin: '5px', color: 'black' }} />
                    }
                    <br />
                    <Button onClick={handleOnSubmit} variant="contained" color="primary">Save</Button>
                    
                    <br />
                    <br />
                </StyledAddProduct>
        </AdminTemplate>
    )
}

export default withRouter(EditProduct);

