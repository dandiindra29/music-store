import React, {useEffect, useState} from 'react';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import { TextField } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import styled from "styled-components";
import {currencyFormatter} from '../utils/currency';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import { Link, withRouter, useParams } from 'react-router-dom';
import { Table } from 'react-bootstrap';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { withTheme } from '@emotion/react';
import { useAlert } from 'react-alert';

import { makeTransaction } from '../api/Transaction';

import countries from '../country/country.json';
import Pay from './Pay';
import Navbar from '../components/Navigation/navbar';
import HowToPay from './HowToPay';

const StyledCarousel = styled.div`
    height: auto; 
    background: #EEE; 
    margin: 10px 10px;
    @media (max-width: 700px){
        margin: 5px 5px;
    }
`;

const StyledText = styled.span`
    font-size: 20px;
    @media(max-width: 700px){
        font-size: 12pt;
    }
`;

const StyledProductName = styled.span`
  font-weight: bold;
  font-size: 20px;
  margin-top: 10px;
  text-align: center;
  @media(max-width: 700px){
      font-size: 12pt;
  }
`

const StyledCart = styled.div`
    margin: auto;
    width: 80%;
    @media(max-width: 700px){
        width: 90%;
    }
`;

const SetyledAddress  = styled.div`
    &:hover{
        cursor: pointer;
        text-decoration: underline;
    }
`;

const StyledDelete = styled.div`
    &:hover{
        cursor: pointer;
    }
`;

const StyledCourier = styled.div`

    &:hover{
        cursor: pointer;
        text-decoration: underline;
    }
`;

const StyledTotal = styled.div`
    color: #17172b;
    margin-top: 20px;
`;

const StyledTableItem = styled.div`
    display: flex;
    justify-content: left; 
    align-items: center; 
    height: 80px;
`;


const useStyles = makeStyles(theme =>({
    
    root: {
      maxWidth: 345,
      maxHeight: 250
    },
    media: {
      height: 420,
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
      },
      selectEmpty: {
        marginTop: theme.spacing(2),
      },
      
  }));

const modalStyle = makeStyles(theme => ({
    root: {
        flexGrow: 1,
      },
      menuButton: {
        marginRight: theme.spacing(2),
      },
      title: {
        flexGrow: 1,
      },
      modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      },
      paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
      },
}))

const Checkout = ({ theme }) => {
    const [carts, setCarts] = useState([]);
    const [user, setUser] = useState({});
    const [open, setOpen] = useState(false);
    const [openPay, setOpenPay] = useState(false);
    const classes = useStyles();
    const [totalPrice, setTotalPrice] = useState(0);
    const [paidPrice, setPaidPrice] = useState(0);
    const classesModal = modalStyle();
    const [name, setName] = useState('');
    const [country, setCountry] = useState('');
    const [street, setStreet] = useState('');
    const [city, setCity] = useState('');
    const [province, setProvince] = useState('');
    const [postalCode, setPostalCode] = useState('');
    const [phone, setPhone] = useState('');
    const [email, setEmail] = useState('');
    const [note, setNote] = useState('');
    const alert = useAlert();
    const { color: {primary, second} } = theme;

    const handleChange = (event) => {
        setCountry(event.target.value);
    };

    const handleOpen = () => {
        setOpen(true);
    };
    
    const handleClose = () => {
        setOpen(false);
    };

    const handleOpenPay = e => {
        setPaidPrice(e.target.value)
        setOpenPay(true)
    }

    const handleClosePay = () => {
        setOpenPay(false);
    }

    const transaction = () => {
        makeTransaction(carts, name, country, city, street, province, postalCode, phone, email, note, totalPrice)
        .then(response => {
            window.alert('Thank you for your purchase!');
        })
    }

    useEffect(() => {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
        let state = localStorage["appState"];
        
        let AppState = JSON.parse(state);
        setUser(AppState.user);

        setCountry(countries[0].name)

        axios.get(`/api/shopping_cart/${AppState.user.id}`, {
            headers: { 'Authorization' : 'Bearer '+ AppState.user.access_token}
        }).then(response => {
            const cartData = response.data;
            setCarts(cartData);

            let totalPriceData = 0;
            cartData.map(data => {
                totalPriceData += data.price * data.quantity;
            })

            setTotalPrice(totalPriceData)
        })
        .catch(response => {
            alert.error(<div style={{ color: 'red' }}>Connection Error!</div>)
        });
    },[]);

    useEffect(() => {
        
        if(name == '' || country == '' || street == '' || city == '' || province == '' || postalCode == '' || phone == '' || phone == '' || email == ''){
            return;
        }else if(name.length > 1 && country.length > 1 && street.length > 1 && city.length > 1 && province.length > 1 && postalCode.length > 1 && phone.length > 1 && phone.length > 1 && email.length > 1){
            return
        }else{
            const paypalButton = document.getElementById("paypal-button");
            if(paypalButton){
                paypal.Button.render({
                    // Configure environment
                    env: 'sandbox',
                    client: {
                      sandbox: 'AUC8t8XY6oQc1kztTfl49iwK_O7xlY80eK4ZN27WGTux6v2SRDGRtK_QCuyQSv17f3zOIlPjKmSCTRGI'
                    },
                    // Customize button (optional)
                    locale: 'en_US',
                    style: {
                      size: 'large',
                      color: 'gold',
                      shape: 'rect',
                    },
                
                    // Enable Pay Now checkout flow (optional)
                    commit: true,
                
                    // Set up a payment
                    payment: function(data, actions) {
                        let newCart = [];
                        newCart = carts.map(cart => {
                            return {
                                name: cart.name,
                                quantity: cart.quantity.toString(),
                                price: cart.price.toString(),
                                currency: 'USD'
                            }
                        })
    
                        return actions.payment.create({
                          transactions: [{
                            amount: {
                              total: totalPrice.toString(),
                              currency: 'USD',
                              details: {
                                subtotal: totalPrice.toString()
                              }
                            },
                            description: 'The payment transaction description.',
                            custom: '90048630024435',
                            //invoice_number: '12345', Insert a unique invoice number
                            payment_options: {
                              allowed_payment_method: 'INSTANT_FUNDING_SOURCE'
                            },
                            soft_descriptor: 'ECHI5786786',
                            item_list: {
                              items: newCart,
                            }
                          }],
                          note_to_payer: 'Contact us for any questions on your order.'
                        });
                      },
                    
                    // Execute the payment
                    onAuthorize: function(data, actions) {
                      return actions.payment.execute().then(function(response) {
                        transaction();
                       
                      });
                    }
                  }, '#paypal-button');
            }
            
        }
    }, [country, street, city, province, postalCode, phone, phone, email]);

    const responsive = {
        superLargeDesktop: {
          // the naming can be any, depends on you.
          breakpoint: { max: 4000, min: 3000 },
          items: 1
        },
        desktop: {
          breakpoint: { max: 3000, min: 1024 },
          items: 1
        },
        tablet: {
          breakpoint: { max: 1024, min: 464 },
          items: 1
        },
        mobile: {
          breakpoint: { max: 464, min: 0 },
          items: 1
        }
      };

    return (
        <div>
            <Navbar />
            <div style={{ marginTop: '120px' }} >
                <StyledCart>
                <h4>
                    <Link to="/my_shopping_cart" style={{ color: `${primary}`, textDecoration: 'unset' }}>
                        <span>SHOPPING CART</span>
                    </Link>
                    <span style={{ fontWeight: 'bold', color: `${second}` }}> {' >'} CHECKOUT</span>
                    <Link to="/orders_complete" style={{ color: `${primary}`, textDecoration: 'unset' }}>
                        <span > {' >'} ORDERS COMPLETE</span>
                    </Link>
                </h4>
                    <hr />
                    <Grid container spacing={2}>
                        <Grid item xs={12} sm={12} md={7} xl={7}>
                            <Card style={{ padding: '10px' }}>
                                <h6 style={{ fontWeight: 'bold' }}>DETAIL TAGIHAN</h6>
                                <p>(Isi form di bawah untuk malanjutkan transaksi)</p>
                                <Grid style={{ marginTop: '10px' }} container spacing={2}>
                                    <Grid item xs={12} sm={12} md={6} lg={6}>
                                    <TextField style={{ color: `${primary}` }} label="Nama" variant="outlined" value={name} onChange={(e) => {
                                        setName(e.target.value);
                                    }} />
                                    </Grid>
                                </Grid>

                                <FormControl variant="outlined" style={{ marginTop: '20px' }}>
                                    <InputLabel id="demo-simple-select-outlined-label">Negara</InputLabel>
                                    <Select
                                    labelId="demo-simple-select-outlined-label"
                                    id="demo-simple-select-outlined"
                                    onChange={handleChange}
                                    value={country}
                                    label="Negara"
                                    >
                                    {
                                        countries.map((country, key) => {
                                            return (
                                                <MenuItem key={key} value={country.name}>{country.name}</MenuItem>
                                            )
                                        })
                                    }
                                    </Select>
                                </FormControl>
                                <br />
                                <br />
                                <TextField label="Alamat Jalan" required variant="outlined" style={{ width: '100%' }} value={street} onChange={(e) => {
                                    setStreet(e.target.value);
                                }} />

                                <br />
                                <br />
                                <TextField label="Kota" required variant="outlined" value={city} onChange={(e) => {
                                    setCity(e.target.value);
                                }} />

                                <br />
                                <br />
                                <TextField label="Provinsi" required variant="outlined" value={province} onChange={(e) => {
                                    setProvince(e.target.value);
                                }} />

                                <br />
                                <br />
                                <TextField label="Kode Pos" required variant="outlined" value={postalCode} onChange={(e) => {
                                    setPostalCode(e.target.value);
                                }} />

                                <br />
                                <br />
                                <TextField label="Telepon" required variant="outlined" value={phone} onChange={(e) => {
                                    setPhone(e.target.value);
                                }} />

                                <br />
                                <br />
                                <TextField label="Alamat Email" required variant="outlined" value={email} onChange={(e) => {
                                    setEmail(e.target.value);
                                }} />

                                <br />
                                <br />
                                <TextField label="Catatan (Opsional)" variant="outlined" style={{ width: '100%' }} value={note} onChange={(e) => {
                                    setNote(e.target.value);
                                }} />
                                

                            </Card>
                        </Grid>
                        <Grid item xs={12} sm={12} md={5} xl={5}>
                            <Card style={{ border: `1px ${second}` }}>
                                <Table >
                                <thead>
                                    <tr>
                                        <th>Pesanan Anda</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><strong>Produk</strong></td>
                                        <td><strong>Subtotal</strong></td>
                                    </tr>
                                    {
                                        carts.map((cart, key) => {
                                            return (
                                                <tr key={key}>
                                                    <td>{cart.name} <span style={{ marginLeft: '10px', fontWeight: 'bolder', color: 'grey' }}>({cart.quantity})</span></td>
                                                    <td><strong>{currencyFormatter(cart.price)}</strong></td>
                                                </tr>
                                            )
                                        })
                                    }
                                    <tr>
                                        <td><strong>Subtotal</strong></td>
                                        <td><strong>{currencyFormatter(totalPrice)}</strong></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Pengiriman</strong></td>
                                        <td>Pengiriman Gratis</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Total</strong></td>
                                        <td><strong>{currencyFormatter(totalPrice)}</strong></td>
                                    </tr>
                                </tbody>
                                </Table>
                                <hr/>
                                <center>
                                <div style={{  width: '95%' }} id="paypal-button"></div>
                                </center>
                                <div style={{ padding: '0px 10px 0px' }}>
                                    <p>
                                    Your personal data will be used to process your order, support your experience throughout this website, and for other purposes described in our kebijakan privasi.
                                    </p>
                                </div>
                                {/* <button style={{margin: '10px', border: 'unset', borderRadius: '8px', width: '90%', background: `linear-gradient(105deg, #17172b, #ffffff)`, color: 'white', fontWeight: 'bold', padding: '5px'}} onClick={handleOpen} value={totalPrice}>Continue To Checkout</button> */}
                            </Card>
                        </Grid>
                    </Grid>
                    <br />
                    <br />
                </StyledCart>
            </div>
            <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classesModal.modal}
            open={open}
            onClose={handleClose}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
                timeout: 500,
            }}
            >
            <Fade in={open}>
                <div className={classesModal.paper}>
                <HowToPay />
                </div>
            </Fade>
            </Modal>

            <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classesModal.modal}
            open={openPay}
            onClose={handleClosePay}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
                timeout: 500,
            }}
            >
            <Fade in={openPay}>
                <div className={classesModal.paper}>
                <Pay totalPrice={paidPrice} />
                </div>
            </Fade>
            </Modal>
        </div>
    )
}

export default withRouter(withTheme(Checkout));