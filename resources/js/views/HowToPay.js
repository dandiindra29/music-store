import React from 'react';
import {Link, withRouter} from 'react-router-dom'

const HowToPay = () => {

    return (
        <div className="content">
            <div className="container">
                <center>
                    <h2>How to Pay</h2>
                </center>
                <hr />
                <ol>
                    <li>Chat on WhatsApp</li>
                    <li>Paypal</li>
                </ol>   
            </div>
        </div>
    )
}

export default withRouter(HowToPay);

