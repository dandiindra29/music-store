import React, {useEffect, useState} from 'react';
import { withRouter, Link } from 'react-router-dom';
import styled from 'styled-components';
import { Table } from 'react-bootstrap';
import Card from '@material-ui/core/Card';
import {currencyFormatter} from '../utils/currency';
import { formatDate } from '../utils/dateFormatter';
import { useAlert } from 'react-alert';

import { transactionComplete } from '../api/Transaction';

import Navbar from '../components/Navigation/navbar';

const StyledOrdersComplete = styled.div`
margin: auto;
width: 80%;
@media(max-width: 700px){
    width: 90%;
}
`;

const OrdersComplete = () => {
    const [transactions, setTransactions] = useState([]);
    const alert = useAlert();

    useEffect(() => {
        let state = localStorage["appState"];
        let AppState = JSON.parse(state);
        if (AppState.isLoggedIn === true) {
          transactionComplete(AppState.user.id, AppState.user.access_token)
          .then(response => {
              const transactionDatas = response.data;
              setTransactions(transactionDatas);
              console.log(transactionDatas);
          })
          .catch(() => {
            alert.error(<div style={{ color: 'red' }}>Connecntion Error!</div>)
          });
        }
    },[])

    return (
        <StyledOrdersComplete>
            <Navbar />
            <div style={{ marginTop: '120px' }}>
            <h4>
                <Link to="/my_shopping_cart" style={{ color: '#D3D3D3', textDecoration: 'unset' }}>
                    <span style={{ fontWeight: 'bold' }}>SHOPPING CART</span>
                </Link>
                <Link to="/checkout" style={{ color: '#D3D3D3', textDecoration: 'unset' }}>
                    <span style={{ fontWeight: 'bold' }}> {' >'} CHECKOUT</span>
                </Link>
                <span style={{ fontWeight: 'bold' }}>{` > ORDERS COMPLETE`}</span>
            </h4>
            <hr />
            <Card>
            <Table responsive hover>
                <thead>
                <tr>
                    <th>No</th>
                    <th>Products</th>
                    <th>Total</th>
                    <th>Date</th>
                    <th>-</th>
                </tr>
                </thead>
                
                <tbody>
                    {
                        transactions.map((transaction, key) => {

                            return (
                                <tr key={key}>
                                    <td>{key+1}</td>
                                    <td>
                                        <ul>
                                        {
                                            transaction.detail.map((d, key) => {
                                                return (
                                                    <li key={key}>{`${d.product_name}`}</li>
                                                )
                                            })
                                        }
                                        </ul>
                                    </td>
                                    <td>
                                        {currencyFormatter(transaction.total_price)}
                                    </td>
                                    <td>
                                        {formatDate(transaction.created_at)}
                                    </td>
                                    <td>
                                        <Link to={`/order_detail/${transaction.id}`}>
                                            <button>Detail</button>
                                        </Link>
                                    </td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </Table>
            </Card>
            </div>
        </StyledOrdersComplete>
    )
}

export default withRouter(OrdersComplete);