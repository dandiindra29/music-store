import React, {useEffect, useState} from 'react';
import ReactDOM from 'react-dom'
import styled from "styled-components";
import { makeStyles } from '@material-ui/core/styles';
import Button from '../components/Button/Button';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import { Link, withRouter, useParams } from 'react-router-dom';
import { withTheme } from '@emotion/react';
import LoadingScreen from 'react-loading-screen';
import { useAlert } from 'react-alert';

import { fetchProduct } from '../api/Product';

import Navbar from '../components/Navigation/navbar';
import NotLoginYet from '../components/NotLoginYet/notLoginYet'
import ShoppingCart from '../components/ShoppingCart/shoppingCart'
import {currencyFormatter} from '../utils/currency';

const StyledImages = styled.div`
  width: 35%;
  height: 350px;
  float: left; 
  position: fixed;
  @media (max-width: 700px) {
    width: 100%;
    height: auto;
    position: relative;
  }
`;

const StyledDescription = styled.div`
  width: 50%; 
  float: right;
  margin-bottom: 70px;
  @media (max-width: 700px) {
    width: 100%;
  }
`

const StyledCarousel = styled.div`
    height: auto; 
    background: #EEE;
    text-align: center; 
    margin: 10px 10px;
    @media (max-width: 700px){
        margin: 5px 5px;
    }
`;

const StyledDetail = styled.div`
    width: 80%;
    margin: auto;
    font-size: 16px;
`;

const StyledText = styled.span`
    font-size: 20px;
    @media(max-width: 700px){
        font-size: 12pt;
    }
`;

const ProductDetail = ({ theme }) => {
    const [product, setProduct] = useState({});
    const [image, setImage] = useState([]);
    const [num, setNum] = useState(2.2);
    const [clicks ,setClicks] = useState(1);
    const [show ,setShow] = useState(true);
    const [dec, setDec] = useState(false);
    const [inc, setInc] = useState(true);
    const [price, setPrice] = useState(0);
    const [productId, setProductId] = useState(0);
    const [stock, setStock] = useState(0);
    const [userId, setUserId] = useState(0);
    const [userAccessToken, setUserAccessToken] = useState('')
    const [loading, setLoading] = useState(true);
    const { productName } = useParams();
    const { color: { primary, second } } = theme;

    const alert = useAlert();

    const StyledProductName = styled.span`
      font-weight: bold;
      color: ${second};
      font-size: 20px;
      margin-top: 10px;
      text-align: center;
      @media(max-width: 700px){
          font-size: 12pt;
      }
    `

    const checkIncr = () => {
        if(clicks === product.quantity - 1){
            setInc(false)
        
          }else{
            setInc(true)
          }
          if(clicks === 2){
            setDec(false)
          }else{
            setDec(true)
          }
    }
  const IncrementItem = () => {
    // checkIncr()
    if(clicks + 1 > product.quantity){
        setClicks(product.quantity)
        
    }else{
      setClicks(clicks + 1 );
    }
  }
  const DecreaseItem = () => {
    // checkIncr()
    if(clicks - 1 < 1){
      setDec(false);
    }else{
      setClicks(clicks - 1 );
    }
  }
  useEffect(() =>{
    
    if(clicks === product.quantity){
        setInc(false)
    
      }else{
        setInc(true)
      }
      if(clicks === 1){
        setDec(false)
      }else{
        setDec(true)
      }
      if(clicks === 0){
        setInc(false);
        setDec(false);
    }
  })

    useEffect(() => {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
        let state = localStorage["appState"];
        let AppState = JSON.parse(state);
        if (AppState.isLoggedIn === true) {
          setUserId(AppState.user.id);
          setUserAccessToken(AppState.user.access_token);
        } 
        fetchProduct(productName).then(response => {
            const product = response.data;
            let im = [];
            im.push(product.thumbnail)
            product.image.map((img, key) => {
                im.push(img) 
            })
            setImage(im);
            setProduct(product);
            setPrice(product.price);
            setProductId(product.id);
            setStock(product.quantity);
            if(product.quantity === 0){
                setClicks(0);
            }
            setLoading(false);
        })
        .catch(response => {
          alert.error(<div style={{ color: 'red' }}>Connection Error!</div>)
          setLoading(false);

        });
        
    }, []);

    const responsive = {
        superLargeDesktop: {
          // the naming can be any, depends on you.
          breakpoint: { max: 4000, min: 3000 },
          items: 1
        },
        desktop: {
          breakpoint: { max: 3000, min: 1024 },
          items: 1
        },
        tablet: {
          breakpoint: { max: 1024, min: 464 },
          items: 1
        },
        mobile: {
          breakpoint: { max: 464, min: 0 },
          items: 1
        }
      };
    
    return (
      <LoadingScreen
            loading={loading}
            bgColor='white'
            spinnerColor={primary}
            textColor={second}
            logoSrc='/image/products/1612153164.jpg'
            // text='Here an introduction sentence (Optional)'
        > 
        <div style={{ height: '100%' }}>
            <Navbar />
            <div style={{ margin: 'auto', marginTop: '90px', width: '80%' }}>
                    <div>
                        <StyledImages>
                        <h2 style={{ marginLeft: '10px', color: `${second}` }}>Product Detail</h2>
                            <Carousel 
                                responsive={responsive}
                                itemClass="carousel-item-padding-10-px"
                            >
                                {
                                    image.map((img, key) => {
                                        return (
                                            <StyledCarousel key={key}>
                                                <img src={img} style={{ height: 'auto', width: '100%' }} />
                                            </StyledCarousel>
                                        )
                                    })
                                }
                                
                            </Carousel>
                        </StyledImages>
                     
                        <StyledDescription >
                        <Card>
                            <StyledDetail>
                            <br />
                            <center>
                            <StyledProductName>
                            {product.name}
                            </StyledProductName>
                            </center>
                            <hr></hr>
                            
                            </StyledDetail>
                            <Grid container>
                                <Grid item xs={3} sm={3} md={3} lg={3}>
                                    <div style={{ marginLeft: '20px'}}><StyledText>Price</StyledText></div>
                                </Grid>
                                <Grid item xs={3} sm={3} md={3} lg={3}>
                                    <h3 style={{ marginLeft: '50px'}}>{currencyFormatter(price)}</h3>
                                </Grid>
                            </Grid>
                                <hr></hr>
                            <Grid container>
                                <Grid item xs={3} sm={3} md={3} lg={3}>
                                <div style={{ marginLeft: '20px'}}><StyledText>Stock</StyledText></div>
                                </Grid>
                                <Grid item xs={3} sm={3} md={3} lg={3}>
                                    <h3 style={{ marginLeft: '50px'}}>{product.quantity}</h3>
                                </Grid>
                            </Grid>
                            <hr></hr>
                            <Grid container>
                            <div style={{margin: '15px'}} dangerouslySetInnerHTML={{__html: product.description}}>

                            </div>
                            </Grid>
                          </Card>
                        </StyledDescription>
                    </div>
            </div>
            <br />
            <br />
            <br />
            <br />
            {
                userId === 0 &&
                <NotLoginYet /> || <ShoppingCart userAccessToken={userAccessToken} stock={stock} productId={productId} userId={userId} quantity={1} price={product.price} />
            }
        </div>
        </LoadingScreen>
    )
    
}


export default withRouter(withTheme(ProductDetail));


