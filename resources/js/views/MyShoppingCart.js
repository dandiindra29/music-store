import React, {useEffect, useState} from 'react';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import { makeStyles } from '@material-ui/core/styles';
import styled from "styled-components";
import {currencyFormatter} from '../utils/currency';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import WhatsAppIcon from '@material-ui/icons/WhatsApp';
import Paper from '@material-ui/core/Paper';
import { withTheme } from '@emotion/react';
import { Table } from 'react-bootstrap';
import { withRouter, Link } from 'react-router-dom';
import LoadingScreen from 'react-loading-screen';
import { useAlert } from 'react-alert';

import { fetchCart } from '../api/Cart';

import Pay from './Pay';
import Navbar from '../components/Navigation/navbar';
import HowToPay from './HowToPay';


const StyledCart = styled.div`
    margin: auto;
    width: 80%;
    @media(max-width: 700px){
        width: 90%;
    }
`;

const StyledDelete = styled.div`
    &:hover{
        cursor: pointer;
    }
`;

const StyledTableItem = styled.div`
    display: flex;
    justify-content: left; 
    align-items: center; 
    height: 80px;
`;

const modalStyle = makeStyles(theme => ({
    root: {
        flexGrow: 1,
      },
      menuButton: {
        marginRight: theme.spacing(2),
      },
      title: {
        flexGrow: 1,
      },
      modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      },
      paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
      },
}))

const MyShoppingCart = ({ theme }) => {
    const [carts, setCarts] = useState([]);
    const [user, setUser] = useState({});
    const [open, setOpen] = useState(false);
    const [openPay, setOpenPay] = useState(false);
    const [totalPrice, setTotalPrice] = useState(0);
    const [paidPrice, setPaidPrice] = useState(0);
    const [loading, setLoading] = useState(true);
    const { color: {primary, second, linen} } = theme;
    const alert = useAlert();
    const classesModal = modalStyle();

    const handleOpen = () => {
        setOpen(true);
    };
    
    const handleClose = () => {
        setOpen(false);
    };

    const handleOpenPay = e => {
        setPaidPrice(e.target.value)
        setOpenPay(true)
    }

    const handleClosePay = () => {
        setOpenPay(false);
    }

    function deleteItem(e){
        console.log(carts)
        console.log(e);
    }

    useEffect(() => {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
        let state = localStorage["appState"];
        
        let AppState = JSON.parse(state);
        
        setUser(AppState.user);

        fetchCart(AppState.user.id, AppState.user.access_token)
        .then(response => {
            const cartData = response.data;
            setCarts(cartData);

            let weightData = 0;
            let totalPriceData = 0;
            cartData.map(data => {
                weightData += data.weight;
                totalPriceData += data.price * data.quantity;
            })
            setTotalPrice(totalPriceData)
            setLoading(false)
        })
        .catch(response => {
            alert.error(<div style={{ color: 'red' }}>Connection Error!</div>)
            setLoading(false);
        });
    },[]);

    const responsive = {
        superLargeDesktop: {
          // the naming can be any, depends on you.
          breakpoint: { max: 4000, min: 3000 },
          items: 1
        },
        desktop: {
          breakpoint: { max: 3000, min: 1024 },
          items: 1
        },
        tablet: {
          breakpoint: { max: 1024, min: 464 },
          items: 1
        },
        mobile: {
          breakpoint: { max: 464, min: 0 },
          items: 1
        }
      };

    return (
        <LoadingScreen
            loading={loading}
            bgColor='white'
            spinnerColor={primary}
            textColor={second}
            logoSrc='/image/products/1612153164.jpg'
            // text='Here an introduction sentence (Optional)'
        > 
            <Navbar />
            <div style={{ marginTop: '120px' }} >
                <StyledCart>
                        <h4>
                            <span style={{ fontWeight: 'bold', color: `${second}` }}>SHOPPING CART</span>
                            <Link to="/checkout" style={{ color: `${primary}`, textDecoration: 'unset' }}>
                                <span > {' >'} CHECKOUT</span>
                            </Link>
                            <Link to="/orders_complete" style={{ color: `${primary}`, textDecoration: 'unset' }}>
                                <span > {' >'} ORDERS COMPLETE</span>
                            </Link>
                        </h4>

                    <Grid container spacing={2}>
                        <Grid item xs={12} sm={12} md={7} xl={7}>
                            <Card style={{ marginTop: '20px' }}>
                                <Table responsive>
                                <thead>
                                    <tr>
                                        <th>Produk</th>
                                        <th>Harga</th>
                                        <th>Jumlah</th>
                                        <th>Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        carts.map((cart, key) => {
                                            return (
                                                <tr key={key}>
                                                    <td>
                                                        <StyledTableItem>
                                                            <div onClick={() => {
                                                                axios.delete(`/api/shopping_cart/${cart.cart_id}`, {
                                                                    headers: { 'Authorization' : 'Bearer '+ user.access_token}
                                                                })
                                                                .then(response => {
                                                                    let cartsAfterDelete= carts.filter(function(item) {
                                                                        return item.cart_id != cart.cart_id
                                                                    })

                                                                    let totalPriceAfterDelete = totalPrice - cart.price * cart.quantity;
                                                                    setTotalPrice(totalPriceAfterDelete);
                                                                    setCarts(cartsAfterDelete);
                                                                })
                                                                
                                                            }}>
                                                                <StyledDelete>
                                                                    <DeleteForeverIcon />
                                                                </StyledDelete>
                                                            </div>
                                                            <img src={cart.thumbnail} width="80" height="80"/>
                                                            <div style={{ marginLeft: '10px'}}>
                                                                {cart.name}
                                                            </div>
                                                        </StyledTableItem>
                                                    </td>
                                                    <td>
                                                        <StyledTableItem>
                                                            <div>
                                                                <strong>{currencyFormatter(cart.price)}</strong>
                                                            </div>
                                                        </StyledTableItem>
                                                    </td>
                                                    <td>
                                                        <StyledTableItem>
                                                        {cart.quantity}
                                                        </StyledTableItem>
                                                    </td>
                                                    <td>
                                                        <StyledTableItem>
                                                        <strong>{currencyFormatter(cart.price * cart.quantity)}</strong>
                                                        </StyledTableItem>
                                                    </td>
                                                </tr>
                                            )
                                        })
                                    }
                                </tbody>
                                </Table>
                            </Card>
                        </Grid>
                        <Grid item xs={12} sm={12} md={5} xl={5}>
                            <Card style={{ marginTop: '20px' }}>
                                <Table >
                                <thead>
                                    <tr>
                                        <th>Total Keranjang Belanja</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Subtotal</td>
                                        <td><strong>{currencyFormatter(totalPrice)}</strong></td>
                                    </tr>
                                    <tr>
                                        <td>Pengiriman</td>
                                        <td>Free Shipping</td>
                                    </tr>
                                    <tr>
                                        <td>Total</td>
                                        <td><strong>{currencyFormatter(totalPrice)}</strong></td>
                                    </tr>
                                </tbody>
                                </Table>
                                <button style={{margin: '10px', marginTop: '0px', border: 'unset', borderRadius: '8px', width: '90%', background: `${primary}`, color: `${linen}`}} onClick={handleOpen} value={totalPrice}>How To Pay?</button>
                                <br />
                                <Link to="/checkout">
                                    <button style={{margin: '10px', border: 'unset', borderRadius: '8px', width: '90%', background: `${second}`, color: `${linen}`, fontWeight: 'bold', padding: '5px'}} onClick={handleOpen} value={totalPrice}>Continue To Checkout</button>
                                </Link>
                                
                            </Card>
                        </Grid>
                    </Grid>
                    <br />
                    <br />
                </StyledCart>
            </div>
            <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classesModal.modal}
            open={open}
            onClose={handleClose}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
                timeout: 500,
            }}
            >
            <Fade in={open}>
                <div className={classesModal.paper}>
                <HowToPay />
                </div>
            </Fade>
            </Modal>

            <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classesModal.modal}
            open={openPay}
            onClose={handleClosePay}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
                timeout: 500,
            }}
            >
            <Fade in={openPay}>
                <div className={classesModal.paper}>
                <Pay totalPrice={paidPrice} />
                </div>
            </Fade>
            </Modal>
        </LoadingScreen>
    )
}

export default withRouter(withTheme(MyShoppingCart));