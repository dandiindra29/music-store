import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import styled from "styled-components";
import { withTheme } from '@emotion/react'
import Grid from '@material-ui/core/Grid';
import { Card, CardContent, CircularProgress } from '@material-ui/core';
import "react-multi-carousel/lib/styles.css";
import { Link, useParams, withRouter } from 'react-router-dom';
import { TextField } from '@material-ui/core';
import LoadingScreen from 'react-loading-screen';
import { useAlert } from 'react-alert';
import ReactDOM from 'react-dom';
import ReactPaginate from 'react-paginate';
import Pagination from '@material-ui/lab/Pagination';

import { allProducts, productsBySubcategory, fecthSubcategory, searchProduct } from '../api/Product';

import Button from '../components/Button/Button';
import Sidebar from '../components/Navigation/Sidebar';
import {currencyFormatter} from '../utils/currency';
import Navbar from '../components/Navigation/navbar';
import Paper from '../components/Paper/Paper';

const StyledDrawer = styled.div`
  background-color: white;
  width: auto;
`;

const StyledSideBar = styled.div`
    position: fixed;
    width: 15%;
    float: right;
    margin : 100px 10px 0px 0px;
    @media (max-width: 900px){
        transform: translate(-350px, 0);
        transition: transform 0.3s ease-in-out;
        z-index: 99;
        margin: 0;
    }

    @media (max-width: 890px){
        margin : 10px 10px 0px 50px;
    }
`;

const StyledSubcategorySidebar = styled.div`
    border-radius: 8px;
`

const StyledNavList = styled.div`
  width: 100%;
  text-align: center;
  list-style-type: none;
  margin: 0;  
  padding: 0;
  margin-botton: 20px;
`;

const StyledLink = styled.span`
    text-decoration: none;
  `;

const StyledProducts = styled.div`
    display: flex;
    flex-direction: column;
    margin: 10px;
`;

const StyledListProducts = styled.div`
    float: right;
    width: 75%;
    @media (max-width: 700px){
        width: 100%;
    }
`;

const StyledItemName = styled.span`
    font-size: 12px;
`

const StyledSubcategory = styled.span`
    font-size: 11px;
`

const StyledCard = styled.div`
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    width: 100%;
    color: black;
    margin-top: 20px;
`;

const StyledPrice = styled.span`
    font-size: 14px;
    font-weight: bold;
    color: linear-gradient(105deg, #17172b, #ffffff);
`
const Products = ({ theme }) => {
    const [data, setData] = useState([]);
    const [offset, setOffset] = useState(0);
    const [items, setItems] = useState([]);
    const { category } = useParams();
    const [categories, setCategory] = useState([]);
    const [change, setChange] = useState(false);
    const [search, setSearch] = useState('');
    const [subcategory, setSubcategory] = useState([]);
    const [page, setPage] = useState(1);
    const [totalPage, setTotalPage] = useState(1)
    const { color: {primary, second} } = theme;
    const [loading, setLoading] = useState(true);
    const alert = useAlert();

    const useStyles = makeStyles(() => ({
        root: {
            '& .Mui-selected': {
                backgroundColor: second,
                color: second
             },
        },
        ul: {
            "& .MuiPaginationItem-root": {
                color: primary
            },
            '& .Mui-selected': {
                backgroundColor: primary,
                color: 'white'
             },
        },
      }));

    const classes = useStyles();

    const getAllProducts = () => {
        allProducts(category).then(response => {
            console.log(response.data)
            setItems(response.data.data)
            setTotalPage(response.data.total)
            setLoading(false)
        })
        .catch(response => {
            setLoading(false)
            alert.error(<div style={{ color: 'red' }}>Connecntion Error!</div>)
        })
    }

    const getProductsBySubcategory = e => {
        const selectedSubcategory = e.target.value
        productsBySubcategory(selectedSubcategory)
        .then(response => {
            setError(false)
            setItems(response.data.data)
        })
        .catch(response => {
            alert.error(<div style={{ color: 'red' }}>Connecntion Error!</div>)
        })
    }

    const getSubcategory = () => {
        fecthSubcategory(category)
        .then(response => {
            setSubcategory(response.data);
        })
    }

    useEffect(() => {
        axios.get(`/api/category`)
        .then(response => {
            // setError(false)
            setCategory(response.data);
        })
        .catch(response => {
            alert.error(<div style={{ color: 'red' }}>Connection Error!</div>)
        })
    },[])

    
    useEffect(() => {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
        document.title = category + ' | Music Store';
        getSubcategory();
        getAllProducts();
    }, [category])

    const handleOnSubmit = () => {
        if(search.length > 0){
            setChange(true);
            searchProduct(category, search)
            .then(response => {
                setItems(response.data.data)
                setChange(false);
            })
            .catch(response => {
                console.log('tidak ada product');
                setChange(false)
            })
    
        }else{
            setChange(true);
            setNotFound(true);
            setChange(false);
        }
    }

    const handlePageClick = (data) => {
        // let selected = data.selected;
        // let offset = Math.ceil(selected * this.props.perPage);
    
        // this.setState({ offset: offset }, () => {
        //   this.loadCommentsFromServer();
        // });
      };
    return (
        <LoadingScreen
            loading={loading}
            bgColor='white'
            spinnerColor={primary}
            textColor={second}
            logoSrc='/image/products/1612153164.jpg'
            // text='Here an introduction sentence (Optional)'
        > 
        <Paper>
                <Navbar />
                    <StyledSideBar id="drawer">
                        <StyledDrawer>
                            {
                                categories.map((c, key) => {
                                    return (
                                        <Link key={key} to={`/products/${c.category}`}>
                                            <Card style={{ padding: '10px', marginBottom: '10px'}}>
                                                {c.category}
                                            </Card>
                                        </Link>
                                    )
                                })
                            }
                        </StyledDrawer>
                    </StyledSideBar>
                    <StyledListProducts >
                        <Card style={{ marginTop: '100px' }}>
                        <StyledProducts>

                            <TextField style={{ margin: '10px', width: '200px' }} id="search_product" type="text" placeholder={`Search ${category}`} onChange={(e) => {
                                setSearch(e.target.value)
                            }}  />

                            <Button onClick={handleOnSubmit} text="Search" style={{ fontWeight: 'bold', color: 'white', background: `${primary}` }} />
                            {
                                change && <CircularProgress style={{ margin: '5px', color: 'black' }} />
                            }
                            <Grid container spacing={1}>

                            {
                                    items.map((item, key) => {
                                        return(
                                        <Grid item xs={6} sm={6} md={3} lg={3} key={key}>
                                        <Link to={`/product/${item.name}`} style={{ textDecoration: 'none' }}>
                                        <StyledCard>
                                            <div style={{ height: '200px' }}>
                                            <img src={item.thumbnail} style={{ width: '100%', height: '100%' }} />
                                            </div>
                                            <CardContent>
                                                <Grid container>
                                                    <Grid item xs={12} sm={12} md={12} lg={12} >
                                                        <StyledItemName>{item.name}</StyledItemName>
                                                    </Grid>
                                                    <Grid item xs={12} sm={12} md={12} lg={12}>
                                                        <StyledSubcategory>{item.subcategory}</StyledSubcategory>
                                                    </Grid>
                                                    <Grid item xs={12} sm={12} md={12} lg={12} >
                                                        <StyledPrice>{currencyFormatter(item.price)}</StyledPrice>
                                                    </Grid>
                                                </Grid>
                                            </CardContent>
                                        </StyledCard>
                                        </Link>
                                        </Grid>
                                        )
                                    })
                                }
                            </Grid>
                        </StyledProducts>
                        </Card>
                        <Card style={{ marginTop: '20px', padding: '10px' }}>
                            <Pagination count={5} classes={{ ul: classes.ul, }} onChange={(event, value) => {
                                    // setPage(value);
                                    console.log(value)
                            }} />
                        </Card>
                        
                    </StyledListProducts>
                </Paper>
  </LoadingScreen>
    )
}


export default withRouter(withTheme(Products));

