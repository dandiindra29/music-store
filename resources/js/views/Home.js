import React, { Component, useEffect, useState } from 'react';
import styled from 'styled-components';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { Link } from 'react-router-dom';
import Button from '../components/Button/Button';
import Header from '../components/HomeHeader/Header';
import LoadingScreen from 'react-loading-screen';
import { withTheme } from '@emotion/react';

const StyledCategory = styled.div`
    width: 100%;
    padding: 20px;
    margin-top: 10px;
`;

const Home = ({ theme }) => {
  const [userName, setUserName] = useState(null);
  const [isLoggedIn, setIsLoggedId] = useState(false);
  const [user, setUser] = useState({});
  const [category, setCategory] = useState([]);
  const [loading, setLoading] = useState(true);
  const { color: { primary, second } } = theme;

  useEffect(() => {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
    const state = localStorage.appState;
    if (state) {
      const AppState = JSON.parse(state);
      setIsLoggedId(AppState.isLoggedIn);
      setUser(AppState.user);
    }

    axios.get('/api/category')
      .then((response) => {
        setCategory(response.data);
        setLoading(false)
      });
  }, []);
  return (
      <LoadingScreen
            loading={loading}
            bgColor='white'
            spinnerColor={primary}
            textColor={second}
            logoSrc='/image/products/1612153164.jpg'
            // text='Here an introduction sentence (Optional)'
        > 
        {
          loading === false &&
          <Header userName={user.name} />
        }
        <StyledCategory>
          <h2 style={{ textDecoration: 'underline' }}>Our Product</h2>
          <Grid spacing={2} container>

            {
                      category.map((c, key) => (
                        <Grid item xs={6} sm={6} md={3} lg={3} key={key}>
                          <Card>
                            <CardContent>
                              <Grid container>
                                <Grid item xs={12} sm={12} md={6} lg={6}>
                                  <h4>{c.category}</h4>
                                </Grid>
                                <Grid item xs={12} sm={12} md={6} lg={6}>
                                  <Link to={`products/${c.category}`}>
                                    <Button float="right" text="See More" />
                                  </Link>
                                </Grid>
                              </Grid>
                            </CardContent>
                          </Card>
                        </Grid>
                      ))
                  }

          </Grid>
        </StyledCategory>
      </LoadingScreen>
  );
};
export default withTheme(Home);
