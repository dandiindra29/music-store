import React, { useState, useEffect } from 'react';
import {Link, withRouter, useParams} from 'react-router-dom'
import { Table } from 'react-bootstrap';
import Card from '@material-ui/core/Card';
import Navbar from '../components/Navigation/navbar';
import styled from 'styled-components';
import {currencyFormatter} from '../utils/currency';
import {formatDate} from '../utils/dateFormatter';
import { useAlert } from 'react-alert';

import { transactionCompleteDetail } from '../api/Transaction';

const StyledOrderDetail = styled.div`
margin: auto;
width: 80%;
@media(max-width: 700px){
    width: 90%;
}
`;

const OrderDetail = () => {
    const [transaction, setTransaction] = useState(null);
    const { transactionId } = useParams();
    const alert = useAlert();

    useEffect(() => {    
        let state = localStorage["appState"];
        let AppState = JSON.parse(state);
        if (AppState.isLoggedIn === true) {
        
        transactionCompleteDetail(transactionId, AppState.user.access_token)
        .then(response => {
              const transactionData = response.data;
              setTransaction(transactionData);
        })
        .catch(() => {
            alert.error(<div style={{ color: 'red' }}>Connection Error!</div>)
        });
        }
    },[])

    return (
        <StyledOrderDetail>
            <Navbar />
                <Card style={{ border: 'solid 1px black', marginTop: '120px' }}>
                {
                    transaction !== null &&
                    <Table responsive hover>
                    <tbody>
                        <tr>
                            <td>Penerima</td>
                            <td>{transaction.name}</td>
                        </tr>
                        <tr>
                            <td>Tanggal</td>
                            <td>{formatDate(transaction.created_at)}</td>
                        </tr>
                        <tr>
                            <td>Phone</td>
                            <td>{transaction.phone}</td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>{transaction.email}</td>
                        </tr>
                        <tr>
                            <td>Alamat</td>
                            <td>{transaction.street}, {transaction.city}, {transaction.province}, {transaction.country}, {transaction.postal_code}</td>
                        </tr>
                        <tr>
                            <td><strong>Produk :</strong></td>
                            <td></td>
                        </tr>
                        {   
                            transaction.detail.map((t, key) => {
                                return (
                                    <tr key={key}>
                                        <td>{t.product_name} <span style={{ marginLeft: '10px', fontWeight: 'bolder', color: 'grey' }}></span></td>
                                        <td><strong>{currencyFormatter(t.price)} x {t.quantity}</strong></td>
                                    </tr>
                                )
                            })
                        }
                        <tr>
                            <td><strong>Subtotal</strong></td>
                            <td><strong>{currencyFormatter(transaction.total_price)}</strong></td>
                        </tr>
                        <tr>
                            <td><strong>Pengiriman</strong></td>
                            <td>Pengiriman Gratis</td>
                        </tr>
                        <tr>
                            <td><strong>Total</strong></td>
                            <td><strong>{currencyFormatter(transaction.total_price)}</strong></td>
                        </tr>
                    </tbody>
                    </Table>
                }
            </Card>
        </StyledOrderDetail>  
                  
    )
}

export default withRouter(OrderDetail);

