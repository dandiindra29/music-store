import React from 'react';
import styled from 'styled-components';

const StyledPaper = styled.div`
    width: 80%;
    margin: auto;
    @media (max-width: 700px){
        width: 100%;
    }
`
const Paper = ({ children }) => {
    
    return(
        <StyledPaper>
            {children}
        </StyledPaper>
    )
}

export default Paper;