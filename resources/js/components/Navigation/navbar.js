import React, { useState, useEffect } from 'react';
import ToolBar from '@material-ui/core/Toolbar'
import { Button, Backdrop, Modal, Fade, AppBar, Menu, MenuItem} from '@material-ui/core';
import styled from 'styled-components';
import { makeStyles } from '@material-ui/core/styles';
import {Link, withRouter, useHistory} from 'react-router-dom';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import { useTranslation } from 'react-i18next';
import i18next from "i18next";
import { withTheme } from '@emotion/react';

import Login from '../../views/Login/Login'

const StyledLogo = styled.span`
    margin: 20px;
    font-weight: bold;
    font-size: 20px;
    &:focus {
        outline: none;
        box-shadow: none;
    }
`;

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
    modal: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    paper: {
      backgroundColor: theme.palette.background.paper,
      border: '2px solid #000',
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
    },
  })); 

  

const Navbar = ({ theme }) => {
    const classes = useStyles();
    const [isLoggedIn, setIsLoggedId] = useState(false);
    const [user, setUser] = useState({});
    const [open, setOpen] = useState(false);
    const [carts, setCarts] = useState({});
    const history = useHistory();
    const { color: {primary, second} } = theme;
    const [anchorEl, setAnchorEl] = React.useState(null);
    const { t } = useTranslation();

    const handleClick = (event) => {
      setAnchorEl(event.currentTarget);
    };
  
    const handleCloseDropdown = () => {
      setAnchorEl(null);
    };

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

    useEffect(() => {
        let state = localStorage["appState"];
        let AppState = JSON.parse(state);
        if (AppState.isLoggedIn === true) {
          setIsLoggedId(AppState.isLoggedIn);
          setUser(AppState.user);
          // this.setState({ isLoggedIn: AppState.isLoggedIn, user: AppState.user });
          axios.get(`/api/shopping_cart/${AppState.user.id}`, {
            headers: { 'Authorization' : 'Bearer '+ AppState.user.access_token}
          }).then(response => {
              const cartData = response.data;
              setCarts(cartData);
          });
        }
        
      }, []);

    const logout = () => {
        axios.get("/api/auth/logout",
        {headers: { 'Authorization' : 'Bearer '+ user.access_token}}).then(response => {
            let appState = {
              isLoggedIn: false,
              user: {}
            };
            localStorage["appState"] = JSON.stringify(appState);
            history.push('/');
          });
        
    }

    return (
      <div>
        <AppBar style={{ background: `white`, position:"fixed" }} >
                <ToolBar >
                    <StyledLogo className={classes.title}>
                      <Link to="/" style={{ textDecoration: 'none', color: `${second}` }}>
                        Ecommerce
                      </Link> 
                    </StyledLogo>
                    {
                        !isLoggedIn ? <Button onClick={handleOpen} style={{ color: `${primary}` }}>Login</Button> : 
                        <div>
                          <Link to="/my_shopping_cart" style={{ textDecoration: 'none'}}>
                            <Button style={{ color: `${primary}` }} >
                              <ShoppingCartIcon />({carts.length})
                            </Button>
                          </Link>
                           <Button style={{ color: `${primary}` }} aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
                            {t('language')}
                          </Button>
                          <Menu
                            id="simple-menu"
                            anchorEl={anchorEl}
                            keepMounted
                            open={Boolean(anchorEl)}
                            onClose={handleCloseDropdown}
                          >
                            <MenuItem onClick={() => {i18next.changeLanguage('en'); setAnchorEl(null);}}><span></span>English</MenuItem>
                            <MenuItem onClick={() => {i18next.changeLanguage('id'); setAnchorEl(null);}}>Indonesia</MenuItem>
                          </Menu>
                          <Button style={{ color: `${primary}` }} onClick={logout}>{t("logout")}</Button>
                        </div>
                    }
                </ToolBar>
          </AppBar>
        <Modal
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          className={classes.modal}
          open={open}
          onClose={handleClose}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500,
          }}
        >
          <Fade in={open}>
            <div className={classes.paper}>
              <Login />
            </div>
          </Fade>
        </Modal>
      </div>
            
    )
}

export default withRouter(withTheme(Navbar));