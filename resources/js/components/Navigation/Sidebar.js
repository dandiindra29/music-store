import React, { useEffect, useState } from 'react';
import { withTheme } from '@emotion/react';
import styled from '@emotion/styled';
import { Link } from 'react-router-dom';
import { useParams } from 'react-router-dom';

const StyledDrawer = styled.div`
  background-color: white;
  width: auto;
`;

const StyledSideBar = styled.div`
    position: fixed;
    width: 20%;
    float: right;
    margin : 100px 10px 0px 0px;
    @media (max-width: 900px){
        transform: translate(-350px, 0);
        transition: transform 0.3s ease-in-out;
        z-index: 99;
        margin: 0;
    }

    @media (max-width: 890px){
        margin : 10px 10px 0px 50px;
    }
`;

const StyledNavList = styled.ul`
  width: 100%;
  text-align: center;
  list-style-type: none;
  margin: 0;  
  padding: 0;
`;

const StyledLink = styled.span`
    text-decoration: none;
  `;

const Drawer = ({ theme }) => {
  const { color: { primary } } = theme;
  const [categories, setCategory] = useState([]);
  const { category } = useParams();

  const StyledNavItem = styled.li`
  border-radius: 8px;
  width: 100%;
  box-sizing: border-box;
  border-bottom: solid 1px gainsboro;
  line-height: 24px;
  padding: 10px;
  text-transform: uppercase;
  text-decoration: none;
  color: ${primary};
  &:hover {
    color: white;
    background-color: ${primary};
  }
`;

const StyledSubcategory = styled.div`

`

  useEffect(() => {
    axios.get(`/api/category`)
        .then(response => {
            // setError(false)
            console.log(response.data);
            setCategory(response.data);
        })
        .catch(response => {
            console.log(response)
            // setError(true)
        })
  },[])

  return (
      <StyledSideBar id="drawer">
        <StyledDrawer>
          <StyledNavList>
            {
                categories.map((c, key) => {
                    if(c.category === category){
                        return(
                          <div key={key}>
                            <StyledNavItem style={{ color: 'white', backgroundColor: `${primary}` }}>
                                <StyledLink>
                                    {c.category}
                                </StyledLink>
                            </StyledNavItem>
                            {
                              c.subcategories.map((subcategory, key) => {
                                return (
                                  <StyledSubcategory key={key}>
                                    {subcategory.subcategory}
                                  </StyledSubcategory>
                                )
                              })
                            }
                          </div>
                        )
                    }else{
                        return(
                            <Link key={key} to={`/products/${c.category}`} style={{ textDecoration: 'none'}} >
                            <StyledNavItem>
                                <StyledLink>
                                    {c.category}
                                </StyledLink>
                            </StyledNavItem>
                            </Link>
                        )
                    }
                    
                })
            }
          </StyledNavList>
        </StyledDrawer>
      </StyledSideBar>
  )
}


export default withTheme(Drawer)

