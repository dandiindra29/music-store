import React, { useState, useEffect } from 'react';
import {currencyFormatter} from '../../utils/currency';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import styled from 'styled-components';
import { withTheme } from '@emotion/react';
import LinearProgress from '@material-ui/core/LinearProgress';
import { useHistory } from "react-router-dom";

import { addToCart } from '../../api/Cart';

var phantom = {
  display: 'block',
  padding: '20px',
  height: '30px',
  width: '100%',
}

const StyledChart = styled.span`
    font-size: 28px;
    @media(max-width : 700px){
        font-size: 20px;
    }
`;

function ShoppingCart({ theme, children, stock, quantity, price, userId, productId, userAccessToken }) {
    const [quan, setQuantity] = useState(1);
    const [pric, setPrice] = useState(null);
    const [click, setClick] = useState(false);
    const { color: { primary, second } } = theme;
    const history = useHistory();

    let style = {
        backgroundColor: `${primary}`,
        borderTop: "1px solid #E7E7E7",
        color: 'white',
        textAlign: "center",
        padding: "10px",
        position: "fixed",
        left: "0",
        bottom: "0",
        height: "60px",
        width: "100%",
    }

    useEffect(() => {
        setQuantity(quantity);
        setPrice(price)
    })

    const handleOnClick = () => {
        setClick(true);
        
        // formData.append('stock', stock - quantity);

        addToCart(userId, productId, quantity, userAccessToken)
        .then(res => {
            setClick(false);
            history.push('/my_shopping_cart');
        })
    }

    return (
        <div style={{ marginTop: '30px' }}>
            <div style={phantom} />
            <div style={style}>
                    {
                        click === true &&
                        <LinearProgress color={second} />
                    }
                <StyledChart>{ currencyFormatter(pric*quan)}   
                
                    {
                        pric*quan !== 0 &&
                        <span onClick={handleOnClick} style={{ textDecoration: 'none', color: `${second}`, cursor: 'pointer' }}> | Add to Cart <ShoppingCartIcon /></span> 
                    }

                </StyledChart>
            </div>
        </div>
    )
}

export default withTheme(ShoppingCart)