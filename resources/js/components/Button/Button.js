import React from 'react';
import styled from "styled-components";

const StyledButton = styled.button`
    border: unset;
    outline: unset;
    border-radius: 50px;
    background-color: gainsboro;
    color : black;
    padding: 5px;
    font-size: 10px;
    width: 80px;
    &:focus {
        outline: none;
        box-shadow: none;
    }
`;

const Button = ({ text, onClick, value, style }) => {
    return (
        <StyledButton onClick={onClick} value={value} style={style}>
            {text}
        </StyledButton>
    )
}
export default Button;