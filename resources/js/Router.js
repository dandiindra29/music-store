import React from 'react';
import {BrowserRouter, Link, Route, Switch} from 'react-router-dom';
import Home from './views/Home';
import Login from './views/Login/Login';

//Buyer
import Products from './views/Products';
import Register from './views/Register/Register';
import ProductDetail from './views/productDetail';
import MyShoppingCart from './views/MyShoppingCart';
import Checkout from './views/Checkout';
import OrdersComplete from './views/OrdersComplete';
import OrderDetail from './views/OrderDetail';

//Admin
import AdminProducts from './views/Admin/Index';
import AdminCategories from './views/Admin/Categories';
import AddCategory from './views/Admin/AddCategory';
import AddProduct from './views/Admin/AddProduct';
import EditProduct from './views/Admin/EditProduct';


import PrivateRoute from './PrivateRoute'
import Dashboard from './views/user/Dashboard/Dashboard';

const Main = props => (
<Switch>
  {/* Home page */}
  <Route exact path='/' component={Home}/>
  
  {/* LogIn */}
  <Route path='/login' component={Login}/>
  <Route path='/register' component={Register}/>

  {/* Buyer if login */}
  <PrivateRoute path='/dashboard' component={Dashboard}/>
  <PrivateRoute path='/my_shopping_cart' component={MyShoppingCart} />
  <PrivateRoute path="/checkout" component={Checkout} />
  <PrivateRoute path="/orders_complete" component={OrdersComplete} />
  <PrivateRoute path="/order_detail/:transactionId" component={OrderDetail} />

  {/* Buyer */}
  <Route exact path='/products/:category' component={Products}/>
  <Route exact path='/product/:productName' component={ProductDetail} />

  {/* Admin */}
  <Route exact path='/admin' component={AdminProducts} />
  <Route exact path='/admin/categories' component={AdminCategories} />
  <Route exact path='/admin/add_product' component={AddProduct} />
  <Route exact path='/admin/add_category' component={AddCategory} />
  <Route exact path='/admin/edit_product/:name' component={EditProduct} />

</Switch>
);
export default Main;