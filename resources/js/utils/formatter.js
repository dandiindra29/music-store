import dayjs from 'dayjs';

export const currencyFormatter = (money) => {
    if (money != undefined){
        return money.toLocaleString('us-US', {
            style: 'currency',
            currency: 'USD'
        }).slice(0, -3)
    }else{
        console.error('Parameter is undefined or null, parameter val: ', money)
    }
}

// import 'dayjs/locale/id';
// dayjs.locale('id');
export function formatDate(value, format = 'D MMMM YYYY, HH:mm') {
  if (!value) return '-';

  return dayjs(value).format(format);
}

