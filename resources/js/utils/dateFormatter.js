import dayjs from 'dayjs';
// import 'dayjs/locale/id';

// dayjs.locale('id');

export function formatDate(value, format = 'D MMMM YYYY, HH:mm') {
  if (!value) return '-';

  return dayjs(value).format(format);
}

