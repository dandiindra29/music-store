import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Link, Route, Switch} from 'react-router-dom';
import { render } from 'react-dom'
import { transitions, positions, Provider as AlertProvider } from 'react-alert'
import AlertTemplate from 'react-alert-template-mui'
import { ThemeProvider } from '@emotion/react';
import Main from './Router';
import './i18nextConf';

const options = {
  // you can also just use 'bottom center'
  position: positions.MIDDLE,
  offset: '30px',
  // you can also just use 'scale'
  transition: transitions.SCALE
}

const theme = {
  color: {
      primary: '#A46843',
      second: '#370D00',
      third: '#BD8E62',
      linen: '#D5D2C1',
  }
}

class Index extends Component {
  render() {
    return (
      <AlertProvider template={AlertTemplate} {...options}>
        <ThemeProvider theme={theme}>
          <BrowserRouter>
            <Route component={Main} />
          </BrowserRouter>
        </ThemeProvider>
      </AlertProvider>
    );
  }
}
ReactDOM.render(<Index/>, document.getElementById('index'));