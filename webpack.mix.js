const mix = require('laravel-mix');
require('dotenv').config();
let webpack = require('webpack')
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

let dotenvplugin = new webpack.DefinePlugin({
    'process.env': {
        APP_NAME: JSON.stringify(process.env.APP_NAME || 'Default app name'),
        NODE_ENV: JSON.stringify(process.env.NODE_ENV || 'development')
    }
})

mix.react('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');

mix.webpackConfig({
    watchOptions: {
        ignored: /node_modules/
    },
    plugins: [
        dotenvplugin,
    ]
});

//cece

// require('laravel-mix-eslint-config');

// mix
//     .js('resources/assets/js/app.js', 'public/js').eslint({
//         enforce: 'pre',
//         test: ['js', 'react'], // will convert to /\.(js|vue)$/ or you can use /\.(js|vue)$/ by itself. 
//         exclude: ['node_modules', 'some/other/dir'], // will convert to regexp and work. or you can use a regular expression like /node_modules/,
//         loader: 'eslint-loader',
//         options: {
//             fix: true,
//             cache: false,
//             //...
//         }
//     })
//     .less('resources/assets/less/app.less', 'public/css');